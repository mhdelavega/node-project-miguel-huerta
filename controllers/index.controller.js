
const mainGet = (req, res) => {
    // res.send('Hello Upgrade Hub!');
    const pageTitle = 'FX Pedal collection';
    return res.render('index', { title: pageTitle, user: req.user });

  };

module.exports = {mainGet};
