
const Musician = require('../models/Musician.model');


const addPedalPut = async (req, res, next) => {
    try {
        console.log('reqbody' = req.body);
        const MusicianId = req.user._id;
        const pedalId = req.params.id;
        console.log(MusicianId + "musician");
        console.log(pedalId + "pedal");

        const updatedMusician = await Musician.findByIdAndUpdate(
            MusicianId,
            { $addToSet: { pedalFavs: Pedal.findById(pedalId) } },
            { new: true }
        );

        // return res.status(200).json(updatedMusician);
        return res.redirect('/pedals');
        
    } catch(error) {
        next(error);
    }
};


module.exports = {
    addPedalPut,
};