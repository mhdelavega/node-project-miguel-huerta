
const Pedal = require('../models/Pedal.model');

const pedalsGet = async (req, res, next) => {

    try {
        const pedals = await Pedal.find();
        // return res.json(pedals);
        if (req.user.role === "builder"){
            isBuilder = true;
            isMusician = false;
        } else {
            isBuilder = false;
            isMusician = true;
        }
        return res.render('pedals/pedals', {pedals: pedals, isBuilder, isMusician});
    } catch(error) {
        console.log('error', error);
        return next(error);
    }
};

const createPedalGet = (req, res, next) => {
    // return res.status(200).json('get pedal');
    return res.render('pedals/create');
};


const createPedalPost = async (req, res, next) => {
    try {

        const { name, type, company, rating, available, followers, builderFaved } = req.body;

        const image = req.image_url ? req.image_url : 'https://image.shutterstock.com/image-vector/guitar-pedal-icon-260nw-216150994.jpg';

        const newPedal = new Pedal({ name, type, company, image, rating, available, followers, builderFaved });

        const createdPedal = await newPedal.save();

        // return res.status(200).json(createdPedal);
        return res.render('pedals/pedal', { pedal: createdPedal });

    } catch (error) {
        return next(error);
    }
};

const pedalEditPut = async (req, res, next) => {
    try {
        const { id, name } = req.body;
        
        const editedPedal = await Pedal.findByIdAndUpdate(
            id,
            { name },
            { new: true }
        );

        return res.status(200).json(editedPedal);
        
    } catch (error) {
        return next(error);
    }
};

const pedalByIdGet = async (req, res, next) => {

    const { id } = req.params;

    try {
        const pedal = await Pedal.findById(id);
        return res.render('pedals/pedal', { pedal });
        // return res.status(200).json(pedal);
    } catch(error) {
        return next(error);
    }
};

const deleteByIdPost = async (req, res, next) => {
    try {
        const { id } = req.params;
        await Pedal.findByIdAndDelete(id);
        // return res.status(200).json('Pedal deleted')
        return res.redirect('/pedals');
    } catch(error) {
        next(error);
    }
};

const pedalsByTypeGet = async (req, res, next) => {
    const { effect } = req.params;

    try {
        const pedalsByType = await Pedal.find({ type: effect }); 
        return res.status(200).json(pedalsByType);

    } catch(error) {
        console.log('error: ', error);
        return next(error);
    }
};

// const petsByAgeGet = async (req, res) => {
//     const { edad } = req.params;

//     try {
//         const petsByAge = await Pet.find({ age: { $gte: edad } });
//         return res.status(200).json(petsByAge);
//     } catch(error) {
//         console.log('error', error);
//         error.status = 418;
//         return next(error);
//     }
// };

module.exports = {
    pedalsGet,
    createPedalGet,
    createPedalPost,
    pedalEditPut,
    pedalByIdGet,
    deleteByIdPost,
    pedalsByTypeGet,
    // petsByAgeGet,
}
