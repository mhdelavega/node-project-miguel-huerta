
const Builder = require('../models/Builder.model');

const allBuildersGet = async (req, res, next) => {
    try {
        const builders = await Builder.find().populate('pedals');
        return res.render('builders/builders', { builders });
        // return res.status(200).json(builders);
    } catch (error) {
        next(error);
    }
};

// const createBuilderPost = async (req, res, next) => {
//     try {
//         const { name } = req.body;

//         const newBuilder = new Builder({ name });

//         const createdBuilder = await newBuilder.save();

//         return res.status(201).json(createdBuilder);
//     } catch (error) {
//         next(error);
//     }
// };

const addPedalPut = async (req, res, next) => {
    try {
        const { BuilderId, pedalId } = req.body;

        const updatedBuilder = await Builder.findByIdAndUpdate(
            BuilderId,
            { $addToSet: { pedals: pedalId } },
            { new: true }
        );

        return res.status(200).json(updatedBuilder);
        
    } catch(error) {
        next(error);
    }
};

// const builderByIdGet = async (req, res, next) => {

//     const { id } = req.params;

//     try {
//         const builder = await Builder.findById(id).populate('pedals');
//         // return res.status(200).json(builder);
//         return res.render('builder/builder', builder);
//     } catch(error) {
//         return next(error);
//     }
// };

const deleteBuilderByIdPost = async (req, res, next) => {
    try {
        const { id } = req.params;
        await Builder.findByIdAndDelete(id);
        // return res.status(200).json('¡Builder borrado!');
        return res.redirect('/builders');
    } catch(error) {
        next(error);
    }
};

module.exports = {
    allBuildersGet,
    // createBuilderPost,
    addPedalPut,
    // builderByIdGet,
    deleteBuilderByIdPost,
};