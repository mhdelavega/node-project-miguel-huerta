const passport = require('passport');
const { isAuthenticated } = require('../middlewares/auth.middleware');


const registerGet = (req, res, next) => {
    // console.log('función registerGet ejecutada')
        return res.render('auth/register');
    }

const registerPost = (req, res, next) => {

        const done = (error, user) => {

            if (error) {
                return next(error);
            };
    
            req.logIn(user, (error) => {
                if(error) {
                    return next(error);
                }
    
                // return res.status(200).json('usuario registrado');
                // if (req.body.role === 'builder'){
                //     return res.redirect('/builders');
                // }
                return res.redirect('/pedals');
            });
        };
        
        // passport.authenticate('registro', done)(req);  
        passport.authenticate('registro', done)(req);  
    }

const loginGet = (req, res, next) => {
        return res.render('auth/login');
    }

const loginPost = (req, res, next) => {
        passport.authenticate('acceso', (error, user) => {
            if(error) {
                return next(error);
            }
    
            req.logIn(user, (error) => {
                if(error) {
                    return next(error);
                }
    
                return res.redirect('/pedals')
            });
        })(req);
    }

const logoutPost = (req, res, next) => {
        if(req.user) {
            req.logout();
    
            req.session.destroy(() => {
                res.clearCookie('connect.sid');
    
                return res.redirect('/');
            });
    
        } else {
            return res.status(200).json('No había ningún usuario logueado');
        }
    }


module.exports = {
    registerGet,
    registerPost,
    loginGet,
    loginPost,
    logoutPost
};