const mongoose = require('mongoose');
const Pedal = require('../models/Pedal.model');
const { DB_URL, DB_CONFIG } = require('../db');


const pedalsArray = [
  {
    name: 'Tubescreamer',
    type: 'overdrive',
    company: 'Ibanez',
    image: 'https://www.pronorte.es/_files/product/5746/image/ibanez-tubescreamer-ts-808.jpg',
    rating: null,
    available: true,
    followers: null,
    builderFaved: [null],
  },
  {
    name: 'Fuzzface',
    type: 'fuzz',
    company: 'Proco',
    image: 'https://pedalesdeguitarra.com/wp-content/uploads/2017/08/dunlop-dallas-arbiter-fuzz-face-xl.jpg',
    rating: null,
    available: true,
    followers: null,
    builderFaved: [null],
  },

  {
    name: 'Rat',
    type: 'distortion',
    company: 'Proco',
    image: 'https://i.ebayimg.com/images/g/6SkAAOSww-Bacf2A/s-l300.jpg',
    rating: null,
    available: true,
    followers: null,
    builderFaved: [null],
  },

  
];

// const petDocuments = pets.map(pet => new Pet(pet));


mongoose.connect(DB_URL, DB_CONFIG)
    .then(async () => {
        
        console.log('Ejecutando seed Pedal.js');

        const allPedals = await Pedal.find();

        if(allPedals.length) {
            await Pedal.collection.drop();
            console.log('Colección Pedals eliminada con éxito');
        }
    })
    .catch(error => {

        console.log('Error buscando en DB:', error);
    })
    .then(async () => {

        await Pedal.insertMany(pedalsArray);
        console.log('Añadidas nuevos Pedals a DB');
    })
    .catch((error) => {

        console.log('Error insertando Pedals', error)
    })
    .finally(() => {

        mongoose.disconnect()
    });
