
//En este caso no tiene sentido, pero quiero comprobar que se puede meter pedales en musicians
//Musician es una extensión de User, creí que metiendo campos en user se completaría musician :/ :(

const mongoose = require('mongoose');
const Pedal = require('../models/Pedal.model');
const User = require('../models/User.model');
const Musician = require('../models/Musician.model');
const { DB_URL, DB_CONFIG } = require('../db');


const usersArray = [
    {
        username: 'Pepe',
        email: 'pepe@upgrade.com',
        password: '1234Asd',
        pedalFavs: [],
        builderFavs: [],
    },
];

mongoose.connect(DB_URL, DB_CONFIG)
    .then(async () => {
        console.log('Ejecutando seed musicians.js');
        const allPedals = await Pedal.find();

        //No los elimino
        // if(allPedals.length) {
        //     await Musician.collection.drop();
        //     console.log('Colección Musician eliminada con éxito');
        // }
    })
    .catch(error => {
        console.log('Error buscando en DB:', error);
    })
    .then(async () => {

        const allPedals = await Pedal.find();
        usersArray[0].pedalFavs = [allPedals[0]._id, allPedals[1]._id];


        await User.insertMany(usersArray);
        console.log('Añadidos nuevas Users a DB');
        // const todosUsers = await Musician.find();
        // console.log(todosUsers);

    })
    .catch((error) => {
        console.log('Error insertando Users', error);
    })
    .finally(() => {
        mongoose.disconnect()
    });
