const mongoose = require('mongoose');
const User = require('./User.model');

const Schema = mongoose.Schema;

const musicianSchema = new Schema(
  {
    pedalFavs: { type: mongoose.Types.ObjectId, ref: 'Pedals' },
    builderFavs: { type: mongoose.Types.ObjectId, ref: 'Builders' },
  },
  // { timestamps: true,}
);


const Musician =  User.discriminator('Musicians', musicianSchema, );

module.exports = Musician;