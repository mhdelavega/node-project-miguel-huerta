const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const pedalSchema = new Schema(
  {
    name: { type: String, required: true },
    // type: { type: String, required: true },
    type: { type: String, required: true, 
      enum: ['fuzz', 'overdrive', 'distortion','rev/del']}, //default: 'fuzz'},
    company: { type: String, required: true },
    image: {type: String},
    rating: { type: {counter: Number, sum: Number}},
    available: { type: Boolean },
    followers: {type: Number},
    builderFaved: {type: [String]},
  },
  { timestamps: true,}
);

const Pedal = mongoose.model('Pedals', pedalSchema);

module.exports = Pedal;