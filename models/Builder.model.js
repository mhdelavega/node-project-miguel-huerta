const mongoose = require('mongoose');
const User = require('./User.model');


const Schema = mongoose.Schema;

const builderSchema = new Schema(
  {
    pedals: { type: mongoose.Types.ObjectId, ref: 'Pedals' },
    pedalFavs: { type: mongoose.Types.ObjectId, ref: 'Pedals' },
    builderFavs: { type: mongoose.Types.ObjectId, ref: 'Builders' },
  },
  // { timestamps: true,}
);

// const Builder = mongoose.model('Builders', builderSchema);

const Builder =  User.discriminator('Builders', builderSchema, );

module.exports = Builder;