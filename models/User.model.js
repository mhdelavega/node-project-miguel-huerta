const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userOptions = {
    discriminatorKey: 'userType',
    collection: 'usersCollection',
    timestamps: true,
}

const userSchema = new Schema(
    {
        username: { type: String, required: true }, 
        email: { type: String, required: true }, 
        password: { type: String, required: true },
        role: { type: String, required: true, enum: ['builder', 'musician'], default: 'builder'},        
        // pedalFavs: { type: mongoose.Types.ObjectId, ref: 'Pedals' },
        // builderFavs: { type: mongoose.Types.ObjectId, ref: 'Builder' }, 
    },
    {userOptions},
    // { timestamps: true }
);

const User = mongoose.model('Users', userSchema);


module.exports = User;


