const isAuthenticated = (req, res, next) => {

    if (req.isAuthenticated()) {
        return next();
    } else {
        return res.redirect('/auth/login');
    }
};

// const isAdmin = (req, res, next) => {
//     if (req.isAuthenticated()) {
//         if (req.user.role === 'admin') {
//             return next();
//         } else {
//             return res.redirect('/');
//         }
//     } else {
//         return res.redirect('/auth/login');
//     }
// };

const isMusician = (req, res, next) => {
    if (req.isAuthenticated()) {
        if (req.user.role === 'musician') {
            return next();
        } else {
            return res.redirect('/');
        }
    } else {
        return res.redirect('/'); //sacar mensaje por pantalla: "solo permitido a cuentas músico"
    }
};

const isBuilder = (req, res, next) => {
    if (req.isAuthenticated()) {
        console.log(req.user.role);
        if (req.user.role === 'builder') {
            return next();
        } else {
            return res.redirect('/');
        }
    } else {
        return res.redirect('/'); //sacar mensaje por pantalla: "solo permitido a cuentas fabricante"
    }
};

module.exports = {
    isAuthenticated,
    // isAdmin,
    isMusician,
    isBuilder
}