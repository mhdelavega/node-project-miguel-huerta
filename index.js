const express = require('express');
const path = require("path");
const hbs = require('hbs');
const session = require("express-session");
const MongoStore = require("connect-mongo");
const passport = require("passport");
const dotenv = require('dotenv');
dotenv.config();

const auth = require('./middlewares/auth.middleware');

const indexRoutes = require("./routes/index.routes");
const authRoutes = require("./routes/auth.routes");
const pedalsRoutes = require("./routes/pedals.routes");
const buildersRoutes = require("./routes/builders.routes");

const db = require('./db');

require("./authentication");

// const PORT = process.env.PORT || 3000;
const PORT =  3000;

db.connect();

const server = express();

server.use(
  session({
      secret: process.env.SESSION_SECRET || 'upgrade-node',
      resave: false,
      saveUninitialized: false,
      cookie: {
          maxAge: 15 * 24 * 60 * 60 * 1000,
      },
      store: MongoStore.create({ mongoUrl: db.DB_URL }),
  })
);

server.use(passport.initialize());
server.use(passport.session());

server.set('views', path.join(__dirname, 'views'));
server.set('view engine', 'hbs');

hbs.registerHelper('mayorque', (a, b, options) => {
  if (a > b) {
      return options.fn(this);
  } else {
      options.inverse(this);
  }
});

// hbs.registerHelper('displayBuilderCollection', (a, b, options) => {
  
//   aLow = a.toLowerCase();
//   bLow = b.toLowerCase();

//   if (a === b) {
//       return options.fn(this);
//   } else {
//       options.inverse(this);
//   }
// });

hbs.registerHelper('mayus', (string) => {
  return string.toUpperCase();
});

server.use(express.static(path.join(__dirname, 'public')));

server.use(express.json());
server.use(express.urlencoded({ extended: true }));

server.use('/', indexRoutes);
server.use('/pedals', auth.isAuthenticated, pedalsRoutes);
server.use('/builders', auth.isBuilder, buildersRoutes);
server.use('/musicians', auth.isMusician, buildersRoutes);
server.use('/auth', authRoutes);
// server.use("/shelters", auth.isAuthenticated, sheltersRoutes);

server.use('*', (req, res) => {
  const error = new Error('Route not found'); 
  error.status = 404;
  console.log('Ruta no encontrada');
  return res.status(404).json(error.message);
});

server.use((error, req, res, next) => {
  console.log("Error Handler", error);
  const errorStatus = error.status || 500;
  const errorMsg = error.message || "Unexpected Error";
  // console.log(`Se ha recibido un error ${errorStatus - errorMsg}`);
  return res.render('error-view', { status: errorStatus, message: errorMsg });
});

// server.use('/', router);

server.listen(PORT, () => {
   console.log(`Servidor a toda máquina en http://localhost:${PORT}`);
});

