const passport = require('passport');
const User = require('../models/User.model');
const registerStrategy = require('./register-strategy');
// const {registerBuilderStrategy, registerMusicianStrategy} = require('./register-strategy');
const loginStrategy = require('./login-strategy');

passport.serializeUser((user, done) => {
    // console.log(user + 'serialize');

    return done(null, user._id);
});

passport.deserializeUser(async (userId, done) => {
    try {
        const existingUser = await User.findById(userId);
        // console.log(userId + 'deserialize');
        return done(null, existingUser);
    } catch (error) {
        return done(error);
    }
});

passport.use('registro', registerStrategy);

// passport.use('registro', registerMusicianStrategy);
// passport.use('registro', registerBuilderStrategy);

passport.use('acceso', loginStrategy);