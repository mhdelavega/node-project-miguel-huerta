const express = require('express');
const controller = require('../controllers/pedals.controller');
const { upload, uploadToCloudinary } = require('../middlewares/file.middleware');
// const { isAdmin } = require('../middlewares/auth.middleware');
const role = require('../middlewares/auth.middleware');
const router = express.Router();

router.get('/', controller.pedalsGet);

// router.get('/create', controller.createPedalGet);
router.get('/create', role.isBuilder, controller.createPedalGet);
// router.post('/create', controller.createPedalPost);
router.post('/create', [upload.single('image'), uploadToCloudinary], controller.createPedalPost);

router.put('/edit', controller.pedalEditPut);

router.get('/:id', controller.pedalByIdGet);

router.post('/:id', controller.deleteByIdPost);

router.get('/type/:effect', controller.pedalsByTypeGet);

// router.get('/age/:edad', controller.petsByAgeGet);

module.exports = router;

