const express = require('express');

const controller = require('../controllers/builders.controller');
const router = express.Router();

router.get('/', controller.allBuildersGet);

// router.post('/create', controller.createBuilderPost);

router.put('/add', controller.addPedalPut);

// router.get('/:id', controller.builderByIdGet);
router.post('/:id', controller.deleteBuilderByIdPost);

module.exports = router;