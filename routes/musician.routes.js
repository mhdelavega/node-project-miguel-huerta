const express = require('express');
const {addPedalPut} = require('../controllers/musicians.controller');

const router = express.Router();

router.put('/add-pedal', addPedalPut);

module.exports = router;